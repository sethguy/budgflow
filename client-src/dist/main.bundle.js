webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/add-cash-flow/add-cash-flow.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".add-cash-flow-container {\n    position: relative;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    width: 100%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    height: 500px;\n}\n\n.add-cash-flow-top-bar {}\n\n.add-cash-flow-input-container {\n    position: relative;\n    background-color: white;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center\n}\n\n.add-cash-flow-input {\n    font-size: 20px;\n    outline: none;\n    border: none;\n    border-bottom: 1px solid black;\n    position: relative;\n}\n\n.add-cash-flow-scroll {\n    position: relative;\n    overflow: scroll;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n}\n\n.option-top-row {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    width: 100%;\n}\n\n.quick-intsructions {\n    position: relative;\n    color: blue;\n    padding: 20px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/add-cash-flow/add-cash-flow.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"add-cash-flow-top-bar\">\n\n\n</div>\n\n<div class=\"add-cash-flow-input-container\">\n    <button (click)=\"addCashFlow()\" mat-button>add</button>\n\n    <input [(ngModel)]=\"newCashFlow.value\" class=\"add-cash-flow-input\" (keyup)=\"inputKeyEvent($event)\" type=\"number\">\n\n</div>\n<div class=\"add-cash-flow-scroll\">\n\n    <cash-flow-item (onItemEvent)=\"itemEvent($event,i)\" *ngFor=\"let item of cashFlowItems let i = index\" [itemData]=\"item\">\n    </cash-flow-item>\n\n\n    <div *ngIf=\"cashFlowItems.length==0\" class=\"quick-intsructions\">\n\n        <p>Start by adding a \"cash flow\"</p>\n        <p>For money you get, add a <span style=\"color:green\"> positive amount </span></p>\n\n        <p>For money you give out, enter a<span style=\"color:red\"> negative amount </span></p>\n\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/add-cash-flow/add-cash-flow.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCashFlowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__ = __webpack_require__("../../../../angular-2-local-storage/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cash_flow_service__ = __webpack_require__("../../../../../src/app/cash-flow.service.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddCashFlowComponent = /** @class */ (function () {
    function AddCashFlowComponent(localStorageService, cashFlowService) {
        this.localStorageService = localStorageService;
        this.cashFlowService = cashFlowService;
        this.cashFlowItems = this.getStoredCashFlows();
        this.newCashFlow = this.newItem();
    }
    AddCashFlowComponent.prototype.newItem = function (value) {
        return {
            value: value,
            recurring: true,
            recurConfig: {
                startDate: new Date(),
                endDate: false,
                interval: 'month',
            }
        };
    };
    AddCashFlowComponent.prototype.ngOnInit = function () {
    };
    AddCashFlowComponent.prototype.itemEvent = function (event, index) {
        if (event.remove) {
            this.cashFlowItems.splice(index, 1);
            this.updateCashFlows();
        }
        if (event.update) {
            this.updateCashFlows();
        }
    };
    AddCashFlowComponent.prototype.updateCashFlows = function () {
        this.cashFlowService.storCashFlows(this.cashFlowItems);
    };
    AddCashFlowComponent.prototype.getStoredCashFlows = function () {
        return this.cashFlowService.getStoredCashFlows();
    };
    AddCashFlowComponent.prototype.addCashFlow = function () {
        if (this.newCashFlow.value) {
            this.cashFlowItems.push(__assign({}, this.newCashFlow));
            this.newCashFlow = this.newItem();
            this.updateCashFlows();
        }
    };
    AddCashFlowComponent.prototype.inputKeyEvent = function (event) {
        if (event.keyCode === 13) {
            this.addCashFlow();
        }
    };
    AddCashFlowComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'add-cash-flow',
            template: __webpack_require__("../../../../../src/app/add-cash-flow/add-cash-flow.component.html"),
            styles: [__webpack_require__("../../../../../src/app/add-cash-flow/add-cash-flow.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__["LocalStorageService"],
            __WEBPACK_IMPORTED_MODULE_2__cash_flow_service__["a" /* CashFlowService */]])
    ], AddCashFlowComponent);
    return AddCashFlowComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".app-component {\n    position: relative;\n    width: 100%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-wrap: wrap;\n        flex-wrap: wrap;\n}\n\n.app-side-bar {\n    position: relative;\n    height: 100%;\n    min-width: 300px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n\n.app-side-bar .main-menu-container {\n    padding: 10px;\n}\n\n.app-time-container {\n    position: relative;\n    height: 100%;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    overflow: hidden;\n    min-width: 300px;\n    height: 100%;\n}\n\n.main-menu-container {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    border-bottom: 1px solid black;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-component\">\n\n    <div class=\"app-side-bar\">\n\n        <div class=\"main-menu-container\">\n            <img class=\"logo\" src=\"/assets/images/budgflow_logo.png\" alt=\"\"> &nbsp;\n            <span>budgFLow by </span> &nbsp;\n\n            <a class=\"jistu-font-logo\" href=\"https://jitsu.ninja\">\n\n              jitsu.ninja\n            </a>\n\n        </div>\n\n        <add-cash-flow class=\"add-cash-flow-container\"></add-cash-flow>\n\n    </div>\n\n    <div class=\"app-time-container\">\n\n        <time-view class=\"time-view-component\">\n\n        </time-view>\n\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css"), __webpack_require__("../../../../../src/app/add-cash-flow/add-cash-flow.component.css"), __webpack_require__("../../../../../src/app/time-views/time-view/time-view.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cash_flow_service__ = __webpack_require__("../../../../../src/app/cash-flow.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_button__ = __webpack_require__("../../../material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_select__ = __webpack_require__("../../../material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material_form_field__ = __webpack_require__("../../../material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material_slide_toggle__ = __webpack_require__("../../../material/esm5/slide-toggle.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material_input__ = __webpack_require__("../../../material/esm5/input.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_material_chips__ = __webpack_require__("../../../material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material_icon__ = __webpack_require__("../../../material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_material_datepicker__ = __webpack_require__("../../../material/esm5/datepicker.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__add_cash_flow_add_cash_flow_component__ = __webpack_require__("../../../../../src/app/add-cash-flow/add-cash-flow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__cash_flow_item_cash_flow_item_component__ = __webpack_require__("../../../../../src/app/cash-flow-item/cash-flow-item.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__time_views_time_views_module__ = __webpack_require__("../../../../../src/app/time-views/time-views.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_angular_2_local_storage__ = __webpack_require__("../../../../angular-2-local-storage/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_angular_2_local_storage__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var houseModules = [
    __WEBPACK_IMPORTED_MODULE_17__time_views_time_views_module__["a" /* TimeViewsModule */]
];
var materialModules = [
    __WEBPACK_IMPORTED_MODULE_11__angular_material_icon__["a" /* MatIconModule */],
    __WEBPACK_IMPORTED_MODULE_13__angular_material__["g" /* MatNativeDateModule */],
    __WEBPACK_IMPORTED_MODULE_12__angular_material_datepicker__["a" /* MatDatepickerModule */],
    __WEBPACK_IMPORTED_MODULE_10__angular_material_chips__["a" /* MatChipsModule */],
    __WEBPACK_IMPORTED_MODULE_9__angular_material_input__["b" /* MatInputModule */],
    __WEBPACK_IMPORTED_MODULE_8__angular_material_slide_toggle__["a" /* MatSlideToggleModule */],
    __WEBPACK_IMPORTED_MODULE_4__angular_material_button__["a" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_5__angular_material_select__["a" /* MatSelectModule */], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["i" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_7__angular_material_form_field__["c" /* MatFormFieldModule */]
];

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_15__add_cash_flow_add_cash_flow_component__["a" /* AddCashFlowComponent */],
                __WEBPACK_IMPORTED_MODULE_16__cash_flow_item_cash_flow_item_component__["a" /* CashFlowItemComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_18_angular_2_local_storage__["LocalStorageModule"].withConfig({
                    prefix: 'budgFlow',
                    storageType: 'localStorage'
                })
            ].concat(materialModules, houseModules, [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ]),
            providers: [__WEBPACK_IMPORTED_MODULE_3__cash_flow_service__["a" /* CashFlowService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/cash-flow-item/cash-flow-item.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".cash-flow-item-true {\n    background-color: green;\n}\n\n.cash-flow-item-false {\n    background-color: red;\n}\n\n.cash-flow-item {\n    color: white;\n    font-weight: bold;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    padding: 2px;\n    border-bottom: 1px solid white;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n\n.cash-flow-item-options {\n    position: relative;\n    background-color: white;\n    width: 100%;\n    border: 1px solid black;\n    transition: all .5s ease-in-out;\n    -webkit-transition: all .5s ease-in-out;\n    overflow: hidden;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n}\n\n.showOptions {\n    height: 250px;\n}\n\n.hideOptions {\n    height: 0;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/cash-flow-item/cash-flow-item.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"cash-flow-item cash-flow-item-{{itemData.value>0}}\">\n\n    <button *ngIf=\"optionsHeight==='hideOptions'\" (click)=\"edit()\" mat-icon-button>\n    <mat-icon aria-label=\"edit\">edit</mat-icon>\n  </button>\n\n    <button *ngIf=\"optionsHeight==='showOptions'\" (click)=\"hideOptions()\" mat-icon-button>\n    <mat-icon aria-label=\"done\">done</mat-icon>\n  </button>\n\n    <span> {{itemData.name }} </span> &nbsp;&nbsp;\n\n    <span> {{itemData.value | currency}}</span>\n\n    <button (click)=\"remove()\" mat-icon-button>\n    <mat-icon aria-label=\"delete\">delete</mat-icon>\n  </button>\n\n</div>\n\n<div class=\"cash-flow-item-options {{optionsHeight}}\">\n\n    <div>\n\n        <mat-form-field class=\"example-full-width\">\n            <input matInput (change)=\"sendUpdate()\" placeholder=\"name\" [(ngModel)]=\"itemData.name\">\n        </mat-form-field>\n\n        <mat-form-field class=\"example-full-width\">\n            <input matInput (change)=\"sendUpdate()\" placeholder=\"value\" type=\"number\" [(ngModel)]=\"itemData.value\">\n        </mat-form-field>\n\n        <mat-slide-toggle (change)=\"sendUpdate()\" [(ngModel)]=\"itemData.recurring\"> Recurring ?</mat-slide-toggle>\n\n        <!-- <div *ngIf=\"itemData.recurring\">\n            <mat-form-field>\n                <mat-select (selectionChange)=\"sendUpdate($event)\" [(ngModel)]=\"itemData.recurConfig.interval\" placeholder=\"Recurring\">\n                    <mat-option [value]=\"'month'\">\n\n                        month\n\n                    </mat-option>\n\n                    <mat-option [value]=\"'week'\">\n\n                        week\n\n                    </mat-option>\n\n                </mat-select>\n            </mat-form-field>\n\n        </div> -->\n\n        <br>\n\n        <mat-form-field>\n            <input (dateChange)=\"itemDateChange($event)\" matInput [matDatepicker]=\"picker1\" placeholder=\"start date\" [formControl]=\"startDate\">\n            <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\n            <mat-datepicker #picker1></mat-datepicker>\n        </mat-form-field>\n\n        <mat-slide-toggle (change)=\"sendUpdate()\" [(ngModel)]=\"itemData.showEndDate\"> set End ?</mat-slide-toggle>\n        <br>\n        <br>\n        <mat-form-field *ngIf=\"itemData.showEndDate\">\n            <input (dateChange)=\"itemDateChange($event)\" matInput [matDatepicker]=\"picker2\" placeholder=\"end date\" [formControl]=\"endDate\">\n            <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\n            <mat-datepicker #picker2></mat-datepicker>\n        </mat-form-field>\n\n    </div>\n    <div>\n\n        <button (click)=\"hideOptions()\" mat-button>close</button>\n\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/cash-flow-item/cash-flow-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CashFlowItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CashFlowItemComponent = /** @class */ (function () {
    function CashFlowItemComponent() {
        this.startDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](new Date());
        this.endDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](new Date());
        this.optionsHeight = "hideOptions";
        this.onItemEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    CashFlowItemComponent.prototype.ngOnInit = function () {
        var start = this.itemData.recurConfig.startDate || new Date();
        var end = this.itemData.recurConfig.endDate || new Date();
        this.startDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](start);
        this.endDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](end);
    };
    CashFlowItemComponent.prototype.ngOnChanges = function (changes) {
        this.sendUpdate();
    };
    CashFlowItemComponent.prototype.itemDateChange = function () {
        this.itemData.recurConfig.startDate = this.startDate.value;
        this.itemData.recurConfig.endDate = this.endDate.value;
        this.sendUpdate();
    };
    CashFlowItemComponent.prototype.sendUpdate = function () {
        if (this.itemData.showEndDate === false) {
            this.itemData.recurConfig.endDate = false;
        }
        this.onItemEvent.emit({ update: true, item: this.itemData });
    };
    CashFlowItemComponent.prototype.remove = function () {
        this.onItemEvent.emit({ remove: true });
    };
    CashFlowItemComponent.prototype.edit = function () {
        this.optionsHeight = "showOptions";
    };
    CashFlowItemComponent.prototype.hideOptions = function () {
        this.optionsHeight = "hideOptions";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CashFlowItemComponent.prototype, "itemData", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CashFlowItemComponent.prototype, "index", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], CashFlowItemComponent.prototype, "onItemEvent", void 0);
    CashFlowItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'cash-flow-item',
            template: __webpack_require__("../../../../../src/app/cash-flow-item/cash-flow-item.component.html"),
            styles: [__webpack_require__("../../../../../src/app/cash-flow-item/cash-flow-item.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CashFlowItemComponent);
    return CashFlowItemComponent;
}());



/***/ }),

/***/ "../../../../../src/app/cash-flow.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CashFlowService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__ = __webpack_require__("../../../../angular-2-local-storage/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CashFlowService = /** @class */ (function () {
    function CashFlowService(localStorageService) {
        this.localStorageService = localStorageService;
        this.subject = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["Subject"]();
        this.initialCashFlowReport = {
            start: 0,
            end: 0,
            income: 0,
            spent: 0,
            net: 0
        };
    }
    CashFlowService.prototype.freshCashFLowReport = function (start) {
        if (!start)
            start = 0;
        return __assign({}, this.initialCashFlowReport, { start: start, end: start });
    };
    ;
    CashFlowService.prototype.getStoredCashFlows = function () {
        return this.localStorageService.get('cashFlows') || [];
    };
    CashFlowService.prototype.getMonthlySums = function (startDate, endDate) {
        var _this = this;
        var cashFlows = this.getStoredCashFlows();
        var months = [];
        var milliInDay = 86400000;
        var range = 365 * milliInDay; // this is a year
        endDate = new Date(startDate.getTime() + range);
        var currentDate = new Date(startDate);
        var days = [];
        while (currentDate < endDate) {
            currentDate.setDate(1); // start at the begining of month
            /*while (currentDate.getDay() != 1) {
              currentDate.setDate(currentDate.getDate() + 1);// inner while loop , increase by day till a monday is found
            }*/
            months.push(new Date(currentDate));
            currentDate.setMonth(currentDate.getMonth() + 1); // increase by month each loop
        }
        var start = 0;
        return months.map(function (month, i, months) {
            var report = _this.createReport(cashFlows, start);
            start = report.end;
            return {
                date: month,
                report: report
            };
        });
    };
    CashFlowService.prototype.createReport = function (cashFlows, start) {
        var _this = this;
        return cashFlows.reduce(function (report, cashFlow) {
            var income = report.in + _this.isPositiveFLow(cashFlow);
            var payments = report.out + _this.isNegativeFLow(cashFlow);
            return {
                start: start,
                end: report.end + cashFlow.value,
                in: income,
                out: payments,
                net: income + payments
            };
        }, {
            start: start,
            end: start,
            in: 0,
            out: 0,
            net: 0
        });
    };
    CashFlowService.prototype.isPositiveFLow = function (flow) {
        return flow.value > 0 ? flow.contribution : 0;
    };
    ;
    CashFlowService.prototype.isNegativeFLow = function (flow) {
        return flow.value < 0 ? flow.contribution : 0;
    };
    ;
    CashFlowService.prototype.storCashFlows = function (cashFlows) {
        this.localStorageService.set('cashFlows', cashFlows || []);
        this.subject.next({ update: true, cashFlows: cashFlows });
    };
    CashFlowService.prototype.sortCashFlowsByStartDate = function (cashFlows) {
        return __WEBPACK_IMPORTED_MODULE_3_lodash__["sortBy"](cashFlows, [function (chasFlow) { return new Date(chasFlow.recurConfig.startDate).getTime(); }]);
    };
    CashFlowService.prototype.buildReportViewList = function (report) {
        return Object.keys(report)
            .map(function (reportKey) { return ({
            name: reportKey,
            value: report[reportKey]
        }); });
    };
    CashFlowService.prototype.getNumberOfIntervals = function (interval, _a) {
        var startDate = _a.startDate, endDate = _a.endDate, millis = _a.millis;
        var intervals = 1;
        var monthsInYear = 12;
        var durationInMillis = millis;
        if (interval === "month") {
            var yearDiff = endDate.getYear() - startDate.getYear();
            var monthDiff = endDate.getMonth() - startDate.getMonth();
            intervals = monthDiff + (yearDiff * monthsInYear);
        }
        else if (interval === "week") {
            intervals = __WEBPACK_IMPORTED_MODULE_4_moment_moment__["duration"](durationInMillis).months();
        }
        else if (typeof interval === "number") {
            intervals = Math.floor(durationInMillis / interval);
        }
        return intervals;
    };
    ; //getNumberOfIntervals
    CashFlowService.prototype.mapRecuring = function (cashFlow, config) {
        cashFlow.contribution = cashFlow.value;
        if (cashFlow.recurring) {
            var interval = cashFlow.recurConfig.interval;
            var startDate = config.startDate;
            var endDate = config.endDate;
            var cashFlowEndDate = cashFlow.recurConfig.endDate;
            var endDateMill = endDate.getTime();
            if (cashFlowEndDate && cashFlowEndDate.getTime < endDateMill) {
                // endDateMill = cashFlowEndDate.getTime();
            }
            //var durationInMillis = endDate.getTime() - Math.max(startDate.getTime(), cashFlow.recurConfig.startDate);
            var durationInMillis = endDate.getTime() - startDate.getTime();
            var range = this.getRangeFrom(startDate, durationInMillis, 0);
            cashFlow.contribution =
                (this.getNumberOfIntervals(interval, range)) * cashFlow.value;
        }
        return cashFlow;
    };
    ;
    CashFlowService.prototype.getCashFlowReport = function (cashFlows, config) {
        var _this = this;
        if (!config.startAmount)
            config.startAmount = 0;
        return cashFlows
            .map(function (cashFlow) {
            return _this.mapRecuring(cashFlow, config);
        })
            .reduce(function (report, cashFlow) {
            var income = report.income + _this.isPositiveFLow(cashFlow);
            var spent = report.spent + _this.isNegativeFLow(cashFlow);
            return {
                start: config.startAmount,
                end: report.end + cashFlow.contribution,
                income: income,
                spent: spent,
                net: income + spent
            };
        }, this.freshCashFLowReport(config.startAmount));
    };
    ;
    CashFlowService.prototype.inTimeRange = function (cashFlow, _a) {
        var startDate = _a.startDate, endDate = _a.endDate, millis = _a.millis;
        var cashFlowDate = this.flatDate(new Date(cashFlow.recurConfig.startDate));
        if (cashFlow.recurring) {
            cashFlowDate = this.flatDate(cashFlow.tempRange.startDate);
        }
        return (cashFlowDate.getTime() >= this.flatDate(startDate).getTime() &&
            cashFlowDate.getTime() <= this.flatDate(endDate).getTime());
    };
    ;
    CashFlowService.prototype.setTempRange = function (cashFlow, iteration) {
        if (cashFlow.recurring) {
            var tempRange = this.getRangeFrom(new Date(cashFlow.recurConfig.startDate), cashFlow.recurConfig.interval, iteration);
            cashFlow.tempRange = tempRange;
        }
        return cashFlow;
    };
    ;
    CashFlowService.prototype.getRangeFrom = function (startDate, interval, intervalDistanceFromOriginStartDate) {
        var range = {
            startDate: startDate,
            endDate: startDate,
            millis: 0
        };
        if (interval === "month") {
            var currentMonth = startDate.getMonth() + intervalDistanceFromOriginStartDate;
            var rangeStart = new Date(startDate.getTime());
            rangeStart.setMonth(currentMonth);
            rangeStart.setDate(1);
            var endDate = new Date(rangeStart);
            endDate.setMonth(endDate.getMonth() + 1);
            // new Date(endDate).setDate(0)
            var millis = endDate.getTime() - rangeStart.getTime();
            range = {
                startDate: rangeStart,
                endDate: endDate,
                millis: millis
            };
        }
        else if (typeof interval === "number") {
            var endDate = new Date(startDate.getTime() + (interval + interval * intervalDistanceFromOriginStartDate));
            var millis = endDate.getTime() - startDate.getTime();
            range = {
                startDate: startDate,
                endDate: endDate,
                millis: millis
            };
        }
        return range;
    };
    ;
    CashFlowService.prototype.flatDate = function (d) {
        d.setHours(0, 0, 0, 0);
        return d;
    };
    ;
    CashFlowService.prototype.getReportIntervalsBy = function (cashFlows, _a, interval) {
        var _this = this;
        var startDate = _a.startDate, endDate = _a.endDate, startAmount = _a.startAmount;
        startDate = this.flatDate(startDate);
        endDate = this.flatDate(endDate);
        var rangeInMillis = endDate.getTime() - startDate.getTime();
        var range = this.getRangeFrom(startDate, rangeInMillis, 0);
        var numberOfintervals = this.getNumberOfIntervals(interval, range);
        var reports = [];
        var sortedCashFlows = cashFlows;
        while (numberOfintervals > 0) {
            var range = this.getRangeFrom(startDate, interval, reports.length);
            reports.push({
                range: range
            });
            numberOfintervals--;
        }
        var start = 0;
        return reports.map(function (report, i) {
            range = report.range;
            var fitingCashFLows = cashFlows
                .map(__WEBPACK_IMPORTED_MODULE_3_lodash__["cloneDeep"])
                .filter(function (cashFlow) {
                var intervalDistanceFromOriginStartDate = _this.intervalsFromStartEnd(new Date(cashFlow.recurConfig.startDate), startDate, interval) + i;
                return _this.inTimeRange(_this.setTempRange(cashFlow, intervalDistanceFromOriginStartDate), range);
            });
            var summary = _this.getCashFlowReport(fitingCashFLows, {
                startDate: report.range.startDate,
                endDate: report.range.endDate,
                startAmount: start
            });
            start = summary.end;
            return __assign({}, report, { cashFlows: fitingCashFLows, summary: summary });
        });
    };
    ;
    CashFlowService.prototype.intervalsFromStartEnd = function (start, end, interval) {
        var rangeInMillis = end.getTime() - start.getTime();
        var range = this.getRangeFrom(start, rangeInMillis, 0);
        return this.getNumberOfIntervals(interval, range);
    };
    CashFlowService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__["LocalStorageService"]])
    ], CashFlowService);
    return CashFlowService;
}());



/***/ }),

/***/ "../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\n    <mat-card-header>\n        <mat-card-title>Sign Up /Sign In</mat-card-title>\n    </mat-card-header>\n    <mat-card-content>\n        <mat-form-field>\n            <input matInput placeholder=\"email\">\n        </mat-form-field>\n        <br>\n        <mat-form-field>\n            <input matInput type=\"password\" placeholder=\"password\">\n        </mat-form-field>\n    </mat-card-content>\n    <mat-card-actions>\n        <button mat-button (click)=\"go()\" color=\"primary\">Go</button>\n        <button mat-button (click)=\"close()\" color=\"warn\">Close</button>\n    </mat-card-actions>\n</mat-card>"

/***/ }),

/***/ "../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignInDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sign_in_msg_sign_in_msg_component__ = __webpack_require__("../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var SignInDialogComponent = /** @class */ (function () {
    function SignInDialogComponent(dialogRef, data, dialog) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.dialog = dialog;
    }
    SignInDialogComponent.prototype.ngOnInit = function () {
    };
    SignInDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    SignInDialogComponent.prototype.go = function () {
        this.close();
        this.showMessageDialog('Login Succes! ', 'You are now logged in');
    };
    SignInDialogComponent.prototype.showMessageDialog = function (title, message) {
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_2__sign_in_msg_sign_in_msg_component__["a" /* SignInMsgComponent */], {
            data: {
                title: title,
                message: message
            }
        });
    };
    SignInDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sign-in-dialog',
            template: __webpack_require__("../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.html"),
            styles: [__webpack_require__("../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialogRef */], Object, __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatDialog */]])
    ], SignInDialogComponent);
    return SignInDialogComponent;
}());



/***/ }),

/***/ "../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\n    <mat-card-header>\n        <mat-card-title>{{data.title}}</mat-card-title>\n    </mat-card-header>\n    <mat-card-content>\n        <span>{{data.message}}</span>\n    </mat-card-content>\n    <mat-card-actions>\n        <button mat-button (click)=\"close()\" color=\"warn\">Close</button>\n    </mat-card-actions>\n</mat-card>"

/***/ }),

/***/ "../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignInMsgComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var SignInMsgComponent = /** @class */ (function () {
    function SignInMsgComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    SignInMsgComponent.prototype.ngOnInit = function () {
    };
    SignInMsgComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    SignInMsgComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sign-in-msg',
            template: __webpack_require__("../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.html"),
            styles: [__webpack_require__("../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialogRef */], Object])
    ], SignInMsgComponent);
    return SignInMsgComponent;
}());



/***/ }),

/***/ "../../../../../src/app/time-views/time-view.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeViewService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__ = __webpack_require__("../../../../angular-2-local-storage/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TimeViewService = /** @class */ (function () {
    function TimeViewService(localStorageService) {
        this.localStorageService = localStorageService;
    }
    TimeViewService.prototype.storeTimeRange = function (timeRange) {
        this.localStorageService.set('timeRange', timeRange);
    };
    TimeViewService.prototype.getTimeStoredRange = function () {
        return this.localStorageService.get('timeRange');
    };
    TimeViewService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angular_2_local_storage__["LocalStorageService"]])
    ], TimeViewService);
    return TimeViewService;
}());



/***/ }),

/***/ "../../../../../src/app/time-views/time-view/time-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".time-view-component {\n    position: relative;\n    height: 100%;\n    overflow: scroll;\n}\n\n.month-slice {\n    position: relative;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    border-left: 1px solid black;\n    height: 100%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n\n.month-slice-info {\n    position: relative;\n    width: 100%;\n    border-bottom: 1px solid black;\n    height: 100px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n\n.report-value {\n    position: relative;\n    margin: 0 10px;\n}\n\n.time-display {\n    position: relative;\n    width: 100%;\n    overflow: hidden;\n}\n\n.report-scroll {\n    position: relative;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    border-top: 1px solid;\n    background-color: lightgrey;\n    width: 100%;\n}\n\n.main-menu-container mat-form-field {\n    position: relative;\n    margin: 5px;\n}\n\n.time-display-scoll {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    overflow: scroll;\n}\n\n.main-menu-container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding: 0 20px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/time-views/time-view/time-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-menu-container\">\n\n    <div>\n        <mat-form-field>\n            <input matInput (dateChange)=\"itemDateChange($event)\" [matDatepicker]=\"timeViewStartDatePicker\" placeholder=\"start\" [formControl]=\"timeViewStartDate\">\n            <mat-datepicker-toggle matSuffix [for]=\"timeViewStartDatePicker\"></mat-datepicker-toggle>\n            <mat-datepicker #timeViewStartDatePicker></mat-datepicker>\n        </mat-form-field>\n        <span>     </span>\n        <mat-form-field>\n            <input matInput (dateChange)=\"itemDateChange($event)\" [matDatepicker]=\"timeViewEndDatePicker\" placeholder=\"end\" [formControl]=\"timeViewEndDate\">\n            <mat-datepicker-toggle matSuffix [for]=\"timeViewEndDatePicker\"></mat-datepicker-toggle>\n            <mat-datepicker #timeViewEndDatePicker></mat-datepicker>\n        </mat-form-field>\n\n    </div>\n\n    <button (click)=\"showSignInDialog()\" mat-raised-button>\n\n      Log In\n\n      </button>\n</div>\n\n<div class=\"time-display\">\n\n    <div class=\"time-display-scoll\">\n        <div *ngFor=\"let month of months\" class=\"month-slice\">\n\n            <p>{{month.shortName}} {{month.year}}</p>\n\n            <div class=\"report-scroll\">\n                <div *ngFor=\"let report of month.reports\" class=\"month-slice-info\">\n                    <span class=\"report-value\">   <b>{{report.name}}</b>  </span>\n                    <br>\n                    <span class=\"report-value\">{{report.value | currency }}</span>\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/time-views/time-view/time-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cash_flow_service__ = __webpack_require__("../../../../../src/app/cash-flow.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__time_view_service__ = __webpack_require__("../../../../../src/app/time-views/time-view.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sign_in_dialog_sign_in_dialog_component__ = __webpack_require__("../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TimeViewComponent = /** @class */ (function () {
    function TimeViewComponent(cashFlowService, timeViewService, dialog) {
        var _this = this;
        this.cashFlowService = cashFlowService;
        this.timeViewService = timeViewService;
        this.dialog = dialog;
        this.timeViewStartDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](new Date());
        this.monthNames = [
            'jan',
            'feb',
            'mar',
            'apr',
            'may',
            'jun',
            'july',
            'aug',
            'sept',
            'oct',
            'nov',
            'dec',
        ];
        this.timeViewEndDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](new Date());
        this.months = [];
        this.cashFlows = [];
        this.cashFlows = this.cashFlowService.getStoredCashFlows();
        this.cashFlowService.subject
            .filter(function (cashFlowEvent) { return cashFlowEvent.update; })
            .subscribe(function (cashFlowEvent) {
            _this.cashFlows = cashFlowEvent.cashFlows;
            _this.getMonthsReports();
        });
        var endDate = new Date(this.timeViewStartDate.value);
        endDate.setMonth(endDate.getMonth() + 12);
        this.timeViewEndDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](endDate);
        var storedTimeRange = this.timeViewService.getTimeStoredRange();
        if (storedTimeRange && storedTimeRange.startDate && storedTimeRange.endDate) {
            this.timeViewStartDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](new Date(storedTimeRange.startDate));
            this.timeViewEndDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](new Date(storedTimeRange.endDate));
        }
        this.getMonthsReports();
    }
    TimeViewComponent.prototype.itemDateChange = function () {
        this.timeViewService.storeTimeRange({
            startDate: this.timeViewStartDate.value,
            endDate: this.timeViewEndDate.value
        });
        this.getMonthsReports();
    };
    TimeViewComponent.prototype.getMonthsReports = function () {
        var _this = this;
        this.months = this.cashFlowService.getReportIntervalsBy(this.cashFlows, {
            startDate: this.timeViewStartDate.value,
            endDate: this.timeViewEndDate.value,
            startAmount: 0
        }, 'month')
            .map(function (monthReport) {
            monthReport.reports = _this.cashFlowService.buildReportViewList(monthReport.summary);
            monthReport.shortName = _this.monthNames[monthReport.range.startDate.getMonth()];
            monthReport.year = monthReport.range.startDate.getYear() - 100;
            return monthReport;
        });
    };
    TimeViewComponent.prototype.ngOnInit = function () {
    };
    TimeViewComponent.prototype.changeTimeSpan = function () {
        console.log(this.timeViewStartDate);
        console.log(this.timeViewEndDate);
    };
    TimeViewComponent.prototype.showSignInDialog = function () {
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__sign_in_dialog_sign_in_dialog_component__["a" /* SignInDialogComponent */]);
    };
    TimeViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'time-view',
            template: __webpack_require__("../../../../../src/app/time-views/time-view/time-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/time-views/time-view/time-view.component.css"), __webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__cash_flow_service__["a" /* CashFlowService */],
            __WEBPACK_IMPORTED_MODULE_3__time_view_service__["a" /* TimeViewService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["d" /* MatDialog */]])
    ], TimeViewComponent);
    return TimeViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/time-views/time-views.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeViewsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_select__ = __webpack_require__("../../../material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_form_field__ = __webpack_require__("../../../material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_slide_toggle__ = __webpack_require__("../../../material/esm5/slide-toggle.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material_input__ = __webpack_require__("../../../material/esm5/input.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material_chips__ = __webpack_require__("../../../material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__ = __webpack_require__("../../../material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__time_view_time_view_component__ = __webpack_require__("../../../../../src/app/time-views/time-view/time-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material_datepicker__ = __webpack_require__("../../../material/esm5/datepicker.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__time_view_service__ = __webpack_require__("../../../../../src/app/time-views/time-view.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__sign_in_dialog_sign_in_dialog_component__ = __webpack_require__("../../../../../src/app/time-views/sign-in-dialog/sign-in-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__sign_in_msg_sign_in_msg_component__ = __webpack_require__("../../../../../src/app/time-views/sign-in-msg/sign-in-msg.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var materialModules = [
    __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["a" /* MatIconModule */],
    __WEBPACK_IMPORTED_MODULE_9__angular_material__["g" /* MatNativeDateModule */],
    __WEBPACK_IMPORTED_MODULE_11__angular_material_datepicker__["a" /* MatDatepickerModule */],
    __WEBPACK_IMPORTED_MODULE_7__angular_material_chips__["a" /* MatChipsModule */],
    __WEBPACK_IMPORTED_MODULE_6__angular_material_input__["b" /* MatInputModule */],
    __WEBPACK_IMPORTED_MODULE_5__angular_material_slide_toggle__["a" /* MatSlideToggleModule */],
    __WEBPACK_IMPORTED_MODULE_9__angular_material__["e" /* MatDialogModule */],
    __WEBPACK_IMPORTED_MODULE_9__angular_material__["c" /* MatCardModule */],
    __WEBPACK_IMPORTED_MODULE_9__angular_material__["b" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_3__angular_material_select__["a" /* MatSelectModule */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["i" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_4__angular_material_form_field__["c" /* MatFormFieldModule */]
];
var TimeViewsModule = /** @class */ (function () {
    function TimeViewsModule() {
    }
    TimeViewsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            entryComponents: [__WEBPACK_IMPORTED_MODULE_13__sign_in_dialog_sign_in_dialog_component__["a" /* SignInDialogComponent */], __WEBPACK_IMPORTED_MODULE_14__sign_in_msg_sign_in_msg_component__["a" /* SignInMsgComponent */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* CommonModule */]
            ].concat(materialModules),
            providers: [__WEBPACK_IMPORTED_MODULE_12__time_view_service__["a" /* TimeViewService */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_10__time_view_time_view_component__["a" /* TimeViewComponent */], __WEBPACK_IMPORTED_MODULE_13__sign_in_dialog_sign_in_dialog_component__["a" /* SignInDialogComponent */], __WEBPACK_IMPORTED_MODULE_14__sign_in_msg_sign_in_msg_component__["a" /* SignInMsgComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_10__time_view_time_view_component__["a" /* TimeViewComponent */]]
        })
    ], TimeViewsModule);
    return TimeViewsModule;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./mt": "../../../../moment/locale/mt.js",
	"./mt.js": "../../../../moment/locale/mt.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map