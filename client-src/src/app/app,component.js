import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'app-component': {
    'position': 'relative',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'display': 'flex',
    'flexWrap': 'wrap'
  },
  'app-side-bar': {
    'position': 'relative',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'minWidth': [{ 'unit': 'px', 'value': 300 }],
    'display': 'flex',
    'flexDirection': 'column'
  },
  'app-side-bar main-menu-container': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }]
  },
  'app-time-container': {
    'position': 'relative',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'flex': '1',
    'overflow': 'hidden',
    'minWidth': [{ 'unit': 'px', 'value': 300 }],
    'height': [{ 'unit': '%V', 'value': 1 }]
  },
  'main-menu-container': {
    'position': 'relative',
    'display': 'flex',
    'alignItems': 'center',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'black' }]
  }
});
