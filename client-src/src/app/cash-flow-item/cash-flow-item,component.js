import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'cash-flow-item-true': {
    'backgroundColor': 'green'
  },
  'cash-flow-item-false': {
    'backgroundColor': 'red'
  },
  'cash-flow-item': {
    'color': 'white',
    'fontWeight': 'bold',
    'display': 'flex',
    'padding': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 2 }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'white' }],
    'alignItems': 'center'
  },
  'cash-flow-item-options': {
    'position': 'relative',
    'backgroundColor': 'white',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'black' }],
    'transition': 'all .5s ease-in-out',
    'WebkitTransition': 'all .5s ease-in-out',
    'overflow': 'hidden',
    'display': 'flex',
    'flexDirection': 'column',
    'justifyContent': 'space-between'
  },
  'showOptions': {
    'height': [{ 'unit': 'px', 'value': 250 }]
  },
  'hideOptions': {
    'height': [{ 'unit': 'px', 'value': 0 }]
  }
});
