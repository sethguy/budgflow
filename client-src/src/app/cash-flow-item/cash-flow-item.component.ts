import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'cash-flow-item',
  templateUrl: './cash-flow-item.component.html',
  styleUrls: ['./cash-flow-item.component.css']
})
export class CashFlowItemComponent implements OnInit, OnChanges {

  @Input() itemData: any;

  startDate = new FormControl(new Date());
  endDate = new FormControl(new Date());

  @Input() index: any;

  optionsHeight = "hideOptions";
  @Output() onItemEvent = new EventEmitter()

  constructor() { }

  ngOnInit() {

    var start = this.itemData.recurConfig.startDate || new Date()
    var end = this.itemData.recurConfig.endDate || new Date()

    this.startDate = new FormControl(start);
    this.endDate = new FormControl(end);

  }

  ngOnChanges(changes: SimpleChanges) {
    this.sendUpdate()
  }

  itemDateChange() {

    this.itemData.recurConfig.startDate = this.startDate.value
    this.itemData.recurConfig.endDate = this.endDate.value

    this.sendUpdate()
  }

  sendUpdate() {

    if (this.itemData.showEndDate === false) {

      this.itemData.recurConfig.endDate = false

    }

    this.onItemEvent.emit({ update: true, item: this.itemData })

  }

  remove() {

    this.onItemEvent.emit({ remove: true })
  }

  edit() {
    this.optionsHeight = "showOptions";

  }

  hideOptions() {
    this.optionsHeight = "hideOptions";

  }

}
