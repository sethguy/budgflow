import { TestBed, inject } from '@angular/core/testing';

import { TimeViewService } from './time-view.service';

describe('TimeViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimeViewService]
    });
  });

  it('should be created', inject([TimeViewService], (service: TimeViewService) => {
    expect(service).toBeTruthy();
  }));
});
