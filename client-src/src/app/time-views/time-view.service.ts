import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class TimeViewService {

  constructor(private localStorageService: LocalStorageService) {

  }

  storeTimeRange(timeRange) {

    this.localStorageService.set('timeRange', timeRange)

  }

  getTimeStoredRange() {

    return this.localStorageService.get('timeRange')


  }


}
