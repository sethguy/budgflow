import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CashFlowService } from '../../cash-flow.service';
import { TimeViewService } from '../time-view.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SignInDialogComponent } from '../sign-in-dialog/sign-in-dialog.component';
@Component({
  selector: 'time-view',
  templateUrl: './time-view.component.html',
  styleUrls: ['./time-view.component.css', '../../app.component.css']
})
export class TimeViewComponent implements OnInit {
  timeViewStartDate = new FormControl(new Date());
  monthNames: Array<any> = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'july',
    'aug',
    'sept',
    'oct',
    'nov',
    'dec',
  ]
  timeViewEndDate = new FormControl(new Date());

  months = []
  cashFlows: any = []

  constructor(private cashFlowService: CashFlowService,
    private timeViewService: TimeViewService,
    public dialog: MatDialog) {

    this.cashFlows = this.cashFlowService.getStoredCashFlows()

    this.cashFlowService.subject

      .filter((cashFlowEvent: any) => cashFlowEvent.update)

      .subscribe(cashFlowEvent => {

        this.cashFlows = cashFlowEvent.cashFlows
        this.getMonthsReports()

      })

    var endDate = new Date(this.timeViewStartDate.value);
    endDate.setMonth(endDate.getMonth() + 12)

    this.timeViewEndDate = new FormControl(endDate);


    var storedTimeRange: any = this.timeViewService.getTimeStoredRange()


    if (storedTimeRange && storedTimeRange.startDate && storedTimeRange.endDate) {


      this.timeViewStartDate = new FormControl(new Date(storedTimeRange.startDate))
      this.timeViewEndDate = new FormControl(new Date(storedTimeRange.endDate))

    }

    this.getMonthsReports()

  }

  itemDateChange() {

    this.timeViewService.storeTimeRange({
      startDate: this.timeViewStartDate.value,
      endDate: this.timeViewEndDate.value
    })

    this.getMonthsReports()

  }

  getMonthsReports() {

    this.months = this.cashFlowService.getReportIntervalsBy(this.cashFlows, {
      startDate: this.timeViewStartDate.value,
      endDate: this.timeViewEndDate.value,
      startAmount: 0
    },
      'month'
    )

      .map((monthReport) => {

        monthReport.reports = this.cashFlowService.buildReportViewList(monthReport.summary)
        monthReport.shortName = this.monthNames[monthReport.range.startDate.getMonth()]
        monthReport.year = monthReport.range.startDate.getYear() - 100
        return monthReport

      })

  }

  ngOnInit() {
  }

  changeTimeSpan() {
    console.log(this.timeViewStartDate)

    console.log(this.timeViewEndDate)

  }

  showSignInDialog() {

    let dialogRef = this.dialog.open(SignInDialogComponent);

  }

}
