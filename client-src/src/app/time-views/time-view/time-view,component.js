import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'time-view-component': {
    'position': 'relative',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'overflow': 'scroll'
  },
  'month-slice': {
    'position': 'relative',
    'flex': '1',
    'borderLeft': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'black' }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'display': 'flex',
    'flexDirection': 'column',
    'alignItems': 'center'
  },
  'month-slice-info': {
    'position': 'relative',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'black' }],
    'height': [{ 'unit': 'px', 'value': 100 }],
    'display': 'flex',
    'flexDirection': 'column',
    'alignItems': 'center',
    'justifyContent': 'center'
  },
  'report-value': {
    'position': 'relative',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }]
  },
  'time-display': {
    'position': 'relative',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'overflow': 'hidden'
  },
  'report-scroll': {
    'position': 'relative',
    'flex': '1',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }],
    'backgroundColor': 'lightgrey',
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'main-menu-container mat-form-field': {
    'position': 'relative',
    'margin': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'time-display-scoll': {
    'position': 'relative',
    'display': 'flex',
    'overflow': 'scroll'
  },
  'main-menu-container': {
    'display': 'flex',
    'justifyContent': 'space-between',
    'alignItems': 'center',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 20 }]
  }
});
