import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SignInMsgComponent } from '../sign-in-msg/sign-in-msg.component';


@Component({
  selector: 'app-sign-in-dialog',
  templateUrl: './sign-in-dialog.component.html',
  styleUrls: ['./sign-in-dialog.component.css']
})
export class SignInDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SignInDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) { }

  ngOnInit() {


  }

  close(): void {
    this.dialogRef.close();
  }

  go(){

    this.close();

    this.showMessageDialog('Login Succes! ','You are now logged in')

  }

  showMessageDialog(title,message){

    let dialogRef = this.dialog.open(SignInMsgComponent,{
      data:{
        title,
        message
      }
    });

  }
}
