import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material';

import { TimeViewComponent } from './time-view/time-view.component';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { TimeViewService } from './time-view.service';
import { SignInDialogComponent } from './sign-in-dialog/sign-in-dialog.component';

import { MatButtonModule,MatDialogModule ,MatCardModule} from '@angular/material';
import { SignInMsgComponent } from './sign-in-msg/sign-in-msg.component';
var materialModules = [
  MatIconModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatChipsModule,
  MatInputModule,
  MatSlideToggleModule,
  MatDialogModule,
  MatCardModule,
  MatButtonModule, MatSelectModule, FormsModule, ReactiveFormsModule, MatFormFieldModule

]
@NgModule({
  entryComponents:[SignInDialogComponent,SignInMsgComponent],
  imports: [
    FormsModule,
    CommonModule,
    ...materialModules
  ],
  providers:[TimeViewService],
  declarations: [TimeViewComponent, SignInDialogComponent, SignInMsgComponent],
  exports:[TimeViewComponent]
})
export class TimeViewsModule { }
