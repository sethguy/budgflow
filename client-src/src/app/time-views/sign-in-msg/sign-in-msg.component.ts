import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-sign-in-msg',
  templateUrl: './sign-in-msg.component.html',
  styleUrls: ['./sign-in-msg.component.css']
})
export class SignInMsgComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SignInMsgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
  close(): void {
    this.dialogRef.close();
  }
}
