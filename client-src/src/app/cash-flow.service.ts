import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { Subject } from 'rxjs';
import * as _ from 'lodash'
import * as moment from 'moment/moment';
import { duration } from 'moment/moment';
@Injectable()
export class CashFlowService {
  subject = new Subject();
  constructor(private localStorageService: LocalStorageService) { }

  initialCashFlowReport = {
    start: 0,
    end: 0,
    income: 0,
    spent: 0,
    net: 0
  };

  freshCashFLowReport(start?) {

    if (!start) start = 0;

    return {
      ...this.initialCashFlowReport,
      start: start,
      end: start
    };
  };



  getStoredCashFlows() {

    return this.localStorageService.get('cashFlows') || []

  }

  getMonthlySums(startDate, endDate) {

    var cashFlows = this.getStoredCashFlows()

    var months = [];

    var milliInDay = 86400000;
    var range = 365 * milliInDay // this is a year
    endDate = new Date(startDate.getTime() + range);
    var currentDate = new Date(startDate);
    var days = [];
    while (currentDate < endDate) {
      currentDate.setDate(1);// start at the begining of month
      /*while (currentDate.getDay() != 1) {
        currentDate.setDate(currentDate.getDate() + 1);// inner while loop , increase by day till a monday is found
      }*/
      months.push(new Date(currentDate));
      currentDate.setMonth(currentDate.getMonth() + 1);// increase by month each loop
    }

    var start = 0

    return months.map((month, i, months) => {

      var report = this.createReport(cashFlows, start)

      start = report.end;

      return {
        date: month,
        report
      }

    })

  }

  createReport(cashFlows, start) {

    return cashFlows.reduce((report, cashFlow) => {

      var income = report.in + this.isPositiveFLow(cashFlow)

      var payments = report.out + this.isNegativeFLow(cashFlow)

      return {

        start: start,

        end: report.end + cashFlow.value,

        in: income,

        out: payments,

        net: income + payments

      }

    }, {
        start: start,
        end: start,
        in: 0,
        out: 0,
        net: 0
      })

  }

  isPositiveFLow(flow) {
    return flow.value > 0 ? flow.contribution : 0;
  };

  isNegativeFLow(flow) {
    return flow.value < 0 ? flow.contribution : 0;
  };

  storCashFlows(cashFlows) {

    this.localStorageService.set('cashFlows', cashFlows || [])

    this.subject.next({ update: true, cashFlows })

  }

  sortCashFlowsByStartDate(cashFlows) {

    return _.sortBy(cashFlows, [function (chasFlow) { return new Date(chasFlow.recurConfig.startDate).getTime(); }]);

  }

  buildReportViewList(report) {

    return Object.keys(report)
      .map(reportKey => ({
        name: reportKey,
        value: report[reportKey]
      }))

  }

  getNumberOfIntervals(interval, { startDate, endDate, millis }) {
    var intervals = 1;
    var monthsInYear = 12;

    var durationInMillis = millis

    if (interval === "month") {

      var yearDiff = endDate.getYear() - startDate.getYear();

      var monthDiff = endDate.getMonth() - startDate.getMonth();

      intervals = monthDiff + (yearDiff * monthsInYear)

    } else if (interval === "week") {
      intervals = moment.duration(durationInMillis).months();
    } else if (typeof interval === "number") {
      intervals = Math.floor(durationInMillis / interval);
    }

    return intervals;
  }; //getNumberOfIntervals



  mapRecuring(cashFlow, config) {
    cashFlow.contribution = cashFlow.value;

    if (cashFlow.recurring) {
      var interval = cashFlow.recurConfig.interval;

      var startDate = config.startDate;

      var endDate = config.endDate;

      var cashFlowEndDate = cashFlow.recurConfig.endDate;

      var endDateMill = endDate.getTime();

      if (cashFlowEndDate && cashFlowEndDate.getTime < endDateMill) {
        // endDateMill = cashFlowEndDate.getTime();
      }

      //var durationInMillis = endDate.getTime() - Math.max(startDate.getTime(), cashFlow.recurConfig.startDate);

      var durationInMillis = endDate.getTime() - startDate.getTime();

      var range = this.getRangeFrom(startDate, durationInMillis, 0)

      cashFlow.contribution =
        (this.getNumberOfIntervals(interval, range)) * cashFlow.value;
    }

    return cashFlow;
  };

  getCashFlowReport(cashFlows, config) {
    if (!config.startAmount) config.startAmount = 0;

    return cashFlows

      .map(cashFlow => {
        return this.mapRecuring(cashFlow, config);
      })
      .reduce((report, cashFlow) => {
        var income = report.income + this.isPositiveFLow(cashFlow);

        var spent = report.spent + this.isNegativeFLow(cashFlow);

        return {
          start: config.startAmount,
          end: report.end + cashFlow.contribution,
          income,
          spent,
          net: income + spent
        };
      }, this.freshCashFLowReport(config.startAmount));
  };

  inTimeRange(cashFlow, { startDate, endDate, millis }) {


    var cashFlowDate = this.flatDate( new Date(cashFlow.recurConfig.startDate));

    if(cashFlow.recurring){
     cashFlowDate = this.flatDate(cashFlow.tempRange.startDate);
    }
    return (
      cashFlowDate.getTime() >= this.flatDate(startDate).getTime() &&
      cashFlowDate.getTime() <= this.flatDate(endDate).getTime()
    );
  };

  setTempRange(cashFlow, iteration) {
    if (cashFlow.recurring) {
      var tempRange = this.getRangeFrom(
        new Date(cashFlow.recurConfig.startDate),
        cashFlow.recurConfig.interval,
        iteration
      );
      cashFlow.tempRange = tempRange;
    }
    return cashFlow;
  };

  getRangeFrom(startDate, interval, intervalDistanceFromOriginStartDate) {
    var range = {
      startDate,
      endDate: startDate,
      millis: 0
    };
    if (interval === "month") {
      var currentMonth = startDate.getMonth() + intervalDistanceFromOriginStartDate;

      var rangeStart = new Date(startDate.getTime());

      rangeStart.setMonth(currentMonth);

      rangeStart.setDate(1);

      var endDate = new Date(rangeStart);

      endDate.setMonth(endDate.getMonth() + 1);
      // new Date(endDate).setDate(0)
      var millis = endDate.getTime() - rangeStart.getTime();

      range = {
        startDate: rangeStart,

        endDate,

        millis
      };

    } else if (typeof interval === "number") {
      var endDate = new Date(startDate.getTime() + (interval + interval * intervalDistanceFromOriginStartDate));

      var millis = endDate.getTime() - startDate.getTime();

      range = {
        startDate,

        endDate,

        millis
      };
    }

    return range;
  };

  flatDate(d) {
    d.setHours(0, 0, 0, 0);

    return d;
  };

  getReportIntervalsBy(
    cashFlows,
    { startDate, endDate, startAmount },
    interval
  ) {

    startDate = this.flatDate(startDate)

    endDate = this.flatDate(endDate)

    var rangeInMillis = endDate.getTime() - startDate.getTime();

    var range = this.getRangeFrom(startDate, rangeInMillis, 0)

    var numberOfintervals = this.getNumberOfIntervals(interval, range);

    var reports = [];

    var sortedCashFlows = cashFlows;

    while (numberOfintervals > 0) {
      var range = this.getRangeFrom(startDate, interval, reports.length);

      reports.push({
        range
      });

      numberOfintervals--;
    }

    var start = 0;

    return reports.map((report, i) => {
      range = report.range;

      var fitingCashFLows = cashFlows

        .map(_.cloneDeep)

        .filter(cashFlow => {

          var intervalDistanceFromOriginStartDate = this.intervalsFromStartEnd(new Date(cashFlow.recurConfig.startDate), startDate, interval) + i

          return this.inTimeRange(this.setTempRange(cashFlow, intervalDistanceFromOriginStartDate), range);

        });

      var summary = this.getCashFlowReport(fitingCashFLows, {
        startDate: report.range.startDate,
        endDate: report.range.endDate,
        startAmount: start
      });

      start = summary.end;

      return {
        ...report,

        cashFlows: fitingCashFLows,

        summary
      };
    });
  };

  intervalsFromStartEnd(start, end, interval) {

    var rangeInMillis = end.getTime() - start.getTime();

    var range = this.getRangeFrom(start, rangeInMillis, 0)

    return this.getNumberOfIntervals(interval, range)

  }

}
