import { Component, OnInit, OnChanges, SimpleChanges, Output } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { FormControl } from '@angular/forms';
import { CashFlowService } from '../cash-flow.service';

@Component({
  selector: 'add-cash-flow',
  templateUrl: './add-cash-flow.component.html',
  styleUrls: ['./add-cash-flow.component.css']
})
export class AddCashFlowComponent implements OnInit {

  cashFlowItems: any = this.getStoredCashFlows()

  constructor(private localStorageService: LocalStorageService,
    private cashFlowService: CashFlowService) { }

  newCashFlow: any = this.newItem()

  newItem(value?) {

    return {
      value,
      recurring: true,
      recurConfig: {
        startDate: new Date(),
        endDate: false,
        interval: 'month',
      }
    }
  }

  ngOnInit() {

  }

  itemEvent(event, index) {

    if (event.remove) {

      this.cashFlowItems.splice(index, 1)
      this.updateCashFlows()

    }

    if (event.update) {

      this.updateCashFlows()

    }

  }

  updateCashFlows() {

    this.cashFlowService.storCashFlows(this.cashFlowItems)

  }

  getStoredCashFlows() {

    return this.cashFlowService.getStoredCashFlows();

  }

  addCashFlow() {

    if (this.newCashFlow.value) {

      this.cashFlowItems.push({

        ...this.newCashFlow
      })

      this.newCashFlow = this.newItem()
      this.updateCashFlows()
    }
  }

  inputKeyEvent(event) {

    if (event.keyCode === 13) {

      this.addCashFlow()
    }

  }

}
