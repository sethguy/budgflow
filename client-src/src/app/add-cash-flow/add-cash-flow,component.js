import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'add-cash-flow-container': {
    'position': 'relative',
    'flex': '1',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'display': 'flex',
    'flexDirection': 'column',
    'height': [{ 'unit': 'px', 'value': 500 }]
  },
  'add-cash-flow-top-bar': {
  },
  'add-cash-flow-input-container': {
    'position': 'relative',
    'backgroundColor': 'white',
    'display': 'flex',
    'alignItems': 'center'
  },
  'add-cash-flow-input': {
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'outline': 'none',
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'black' }],
    'position': 'relative'
  },
  'add-cash-flow-scroll': {
    'position': 'relative',
    'overflow': 'scroll',
    'flex': '1'
  },
  'option-top-row': {
    'position': 'relative',
    'display': 'flex',
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'quick-intsructions': {
    'position': 'relative',
    'color': 'blue',
    'padding': [{ 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }]
  }
});
