import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css',
    './add-cash-flow/add-cash-flow.component.css',
    './time-views/time-view/time-view.component.css'
  ]
})
export class AppComponent {

}
