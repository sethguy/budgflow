import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { CashFlowService } from './cash-flow.service';

import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';

import { AppComponent } from './app.component';
import { AddCashFlowComponent } from './add-cash-flow/add-cash-flow.component';
import { CashFlowItemComponent } from './cash-flow-item/cash-flow-item.component';

import { TimeViewsModule } from './time-views/time-views.module'

var houseModules = [
  TimeViewsModule
]

var materialModules = [
  MatIconModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatChipsModule,
  MatInputModule,
  MatSlideToggleModule,
  MatButtonModule, MatSelectModule, FormsModule, ReactiveFormsModule, MatFormFieldModule

]

import { LocalStorageModule } from 'angular-2-local-storage';

@NgModule({
  declarations: [
    AppComponent,
    AddCashFlowComponent,
    CashFlowItemComponent
  ],
  imports: [
    LocalStorageModule.withConfig({
      prefix: 'budgFlow',
      storageType: 'localStorage'
    }),
    ...materialModules,
    ...houseModules,
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [CashFlowService],
  bootstrap: [AppComponent]
})
export class AppModule { }
