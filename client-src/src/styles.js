import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // You can add global styles to this file, and also import other style files
  'import': '"~@angular/material/prebuilt-themes/indigo-pink.css"',
  'html': {
    'position': 'relative',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'body': {
    'position': 'relative',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'logo': {
    'position': 'relative',
    'height': [{ 'unit': 'px', 'value': 50 }]
  },
  'input': {
    'fontFamily': ''BioRhyme Expanded', helvetica !important',
    'fontSize': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': '!important' }]
  },
  'button': {
    'fontFamily': ''BioRhyme Expanded', helvetica !important',
    'fontSize': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': '!important' }]
  },
  'span': {
    'fontFamily': ''BioRhyme Expanded', helvetica !important',
    'fontSize': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': '!important' }]
  },
  'html': {
    'fontFamily': ''BioRhyme Expanded', helvetica !important',
    'fontSize': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': '!important' }]
  },
  'body': {
    'fontFamily': ''BioRhyme Expanded', helvetica !important',
    'fontSize': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': '!important' }]
  },
  'div': {
    'fontFamily': ''BioRhyme Expanded', helvetica !important',
    'fontSize': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': '!important' }]
  },
  'jistu-font-logo': {
    'fontFamily': ''Gloria Hallelujah', cursive !important'
  },
  // font-family: 'BioRhyme Expanded', helvetica !important;
    font-size: 15px !important;

        font-family: 'Josefin Sans', helvetica !important;
    font-size: 18px !important;
});
