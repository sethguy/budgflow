var cashFlows = [{
        value: 4400,
        recurring: true,
        recurConfig: {
            startDate: "2018-02-07T00:55:21.091Z",
            endDate: false,
            interval: "month"
        }
    },
    {
        value: -450,
        recurring: true,
        recurConfig: {
            startDate: "2018-02-07T00:55:40.564Z",
            endDate: false,
            interval: "month"
        }
    },
    {
        value: -400,
        recurring: true,
        recurConfig: {
            startDate: "2018-02-07T00:55:44.282Z",
            endDate: false,
            interval: "month"
        }
    },
    {
        value: -1800,
        recurring: true,
        recurConfig: {
            startDate: "2018-02-07T00:55:54.373Z",
            endDate: false,
            interval: "month"
        }
    },
    {
        value: -400,
        recurring: true,
        recurConfig: {
            startDate: "2018-02-07T01:00:57.028Z",
            endDate: false,
            interval: "month"
        }
    }
];
var initialCashFlowReport = {
    start: 0,
    end: 0,
    income: 0,
    spent: 0,
    net: 0
};

var freshCashFLowReport = function(start) {

    if (!start) start = 0

    return {
        ...initialCashFlowReport,
        start: start,
        end: start
    };
};
var isPositiveFLow = function(flow) {
    return flow.value > 0 ? flow.contribution : 0;
};

var isNegativeFLow = function(flow) {
    return flow.value < 0 ? flow.contribution : 0;
};

var buildReportViewList = function(report) {
    return Object.keys(report).map(reportKey => ({
        name: reportKey,
        value: report[reportKey]
    }));
};

var mapRecuring = function(cashFlow, config) {

    cashFlow.contribution = cashFlow.value;

    if (cashFlow.recurring) {
        var interval = cashFlow.recurConfig.interval;

        var startDate = config.startDate;

        var endDate = config.endDate;

        var cashFlowEndDate = cashFlow.recurConfig.endDate;

        var endDateMill = endDate.getTime();

        if (cashFlowEndDate && cashFlowEndDate.getTime < endDateMill) {
            // endDateMill = cashFlowEndDate.getTime();
        }

        //var durationInMillis = endDate.getTime() - Math.max(startDate.getTime(), cashFlow.recurConfig.startDate);

        var durationInMillis = endDate.getTime() - startDate.getTime();

        cashFlow.contribution = (getNumberOfIntervals(interval, durationInMillis) || 1) * cashFlow.value;
    }

    return cashFlow;
};

var getNumberOfIntervals = function(interval, durationInMillis) {
    var intervals = 1;

    if (interval === "month") {
        intervals = moment.duration(durationInMillis).months();
    } else if (interval === "week") {
        intervals = moment.duration(durationInMillis).months();
    } else if (typeof interval === "number" && interval > 0) {
        intervals = Math.floor(durationInMillis / interval);
    }

    return intervals;
}; //getNumberOfIntervals

var getCashFlowReport = function(cashFlows, config) {

    if (!config.startAmount) config.startAmount = 0

    return cashFlows

        .map(cashFlow => {
            return mapRecuring(cashFlow, config);
        })
        .reduce((report, cashFlow) => {
            var income = report.income + isPositiveFLow(cashFlow);

            var spent = report.spent + isNegativeFLow(cashFlow);

            return {
                start: config.startAmount,
                end: report.end + cashFlow.contribution,
                income,
                spent,
                net: income + spent
            };
        }, freshCashFLowReport(config.startAmount));
};

var milliInDay = 86400000;
var range = 365 * milliInDay; // this is a year
var startDate = new Date();
var endDate = new Date(startDate.getTime() + range);
var currentDate = new Date(startDate);

var report = getCashFlowReport(cashFlows, {
    startDate,
    endDate,
    startAmount: 0
});

console.log("report", report);

var getRangeFrom = function(startDate, interval, iteration) {
    var range = {
        startDate,
        endDate: startDate,
        millis: 0
    };
    if (interval === "month") {
        var currentMonth = startDate.getMonth() + iteration;

        var rangeStart = new Date(startDate.getTime());

        rangeStart.setMonth(currentMonth);

        rangeStart.setDate(1);

        var endDate = new Date(rangeStart);

        endDate.setMonth(endDate.getMonth() + 1);
        // new Date(endDate).setDate(0)
        var millis = endDate.getTime() - rangeStart.getTime();

        range = {
            startDate: rangeStart,

            endDate,

            millis
        };
    } else if (typeof interval === "number" && interval > 0) {
        var endDate = new Date(startDate.getTime() + interval);

        var millis = endDate.getTime() - startDate.getTime();

        range = {
            startDate,

            endDate,

            millis
        };
    }

    return range;
};

var flatDate = function(d) {

    d.setHours(0, 0, 0, 0);

    return d
}

var inTimeRange = function(cashFlow, { startDate, endDate, millis }) {

    var cashFlowDate = flatDate(cashFlow.tempRange.startDate)
    return (cashFlowDate.getTime() >= flatDate(startDate).getTime()) &&

        (cashFlowDate.getTime() <= flatDate(endDate).getTime())

};

var setTempRange = function(cashFlow, iteration) {

    if (cashFlow.recurring) {

        var tempRange = getRangeFrom(new Date(cashFlow.recurConfig.startDate), cashFlow.recurConfig.interval, iteration)

        cashFlow.tempRange = tempRange

    }
    return cashFlow

}

var getReportIntervalsBy = function(
    cashFlows, { startDate, endDate, startAmount },
    interval
) {
    var rangeInMillis = endDate.getTime() - startDate.getTime();

    var numberOfintervals = getNumberOfIntervals(interval, rangeInMillis);

    console.log("intervals", numberOfintervals);

    var reports = [];

    var sortedCashFlows = cashFlows;

    while (numberOfintervals > 0) {

        var range = getRangeFrom(startDate, interval, reports.length);

        reports.push({
            range
        });

        numberOfintervals--;
    }

    var start = 0;

    return reports.map((report, i) => {
        range = report.range;

        var fitingCashFLows = cashFlows

            .map(_.cloneDeep)

        .filter(cashFlow => {

            return inTimeRange(setTempRange(cashFlow, i), range);

        })

        var summary = getCashFlowReport(fitingCashFLows, {

            startDate: report.range.startDate,
            endDate: report.range.endDate,
            startAmount: start

        })
        start = summary.end
        console.log(start, summary.end)
        return {
            ...report,

            cashFlows: fitingCashFLows,

            summary

        };



    });
};


var getStartAmount = function() {



}

var reports = getReportIntervalsBy(
    cashFlows, { startDate, endDate, startAmount: 0 },
    "month"
);

console.log('reports', reports);