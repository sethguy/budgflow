#copying data
gsutil cp [LOCAL_OBJECT_LOCATION] gs://[DESTINATION_BUCKET_NAME]/

#If you want to copy an entire directory tree you need to use the -r option:

gsutil cp -r dir gs://my-bucket


# making data pubilc
gsutil iam ch allUsers:objectViewer gs://[BUCKET_NAME]


#If you have a large number of files to transfer you might want to use the gsutil -m option, to perform a parallel (multi-threaded/multi-processing) copy:

gsutil -m cp -r dir gs://my-bucket